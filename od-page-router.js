import { PolymerElement, html } from '@polymer/polymer';
import { afterNextRender } from '@polymer/polymer/lib/utils/render-status.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';
import './od-page-router-item.js';

export class OdPageRouter extends PolymerElement {
    static get is() { return 'od-page-router' }
    static get template() {
        return html`
            <style>
                :host {
                    display: block;
                    width: 100%;
                }
                #pages-container {
                    display: none;
                }
                #content-container {
                    display: block;
                    width: 100%;
                    height: 100%;
                }
            </style>

            <app-location path="{{_loadRoute}}" query-params="{{_queryParams}}" route="{{_route}}" url-space-regex="^[[baseRoute]]"></app-location>
            <app-route route="{{_route}}" pattern="[[baseRoute]]:area" data="{{routeData}}" tail="{{tailData}}"></app-route>
            <div id="pages-container">
                <slot id="pages-slot"></slot>
            </div>
            <div id="content-container"></div>
        `;
    }
    static get properties() {
        return {
            defaultRoute: {
                type: String
            },

            loadRoute: {
                type: String,
                observer: '_loadSimpleRoute'
            },

            loadedRoute: {
                type: String,
                notify: true
            },

            baseRoute: {
                type: String,
                value: '/'
            },

            lazyPages: Object,

            errorPages: Object,

            _pages: {
                type: Array,
                value: []
            },

            _page: Object
        };
    }
    static get observers() { return [ '_urlRoutePageChanged( tailData.prefix, _queryParams )', '_setLoaded( _loadRoute )' ]; }

    connectedCallback() {
        super.connectedCallback();
        window.addEventListener( "od-auth-done", () => this._removeCodeParam() );
    }

    disconnectedCallback() {
        super.disconnectedCallback();
        window.removeEventListener( "od-auth-done", () => this._removeCodeParam() );
    }

    ready() {
        super.ready();
        this._buildPages();                      
        this.addEventListener( 'od-route-change', this._routeChangeEvent );         
    }

    _buildPages() {
        var node = this.shadowRoot.querySelector( '#pages-slot' );
        var children = node.assignedNodes( {
                flatten: true
            } ).filter( n => n.nodeType === Node.ELEMENT_NODE && n.tagName.toLowerCase() === 'od-page-router-item' );
            
        for ( var i = 0; i < children.length; i++ ) {
            var child = children[ i ];
            var route = '';
            if ( child.route !== undefined && child.route !== null ) {
                route = child.route;
            }                    
            this._buildPagesLoop( child, route );
            this.removeChild( child );
        }
        this._setDefaultRoute();
    }

    _buildPagesLoop( node, routeString ) {
        var newTagName = node.newTagName;
        if ( node.pageName !== undefined && node.pageName !== null && newTagName !== null && newTagName !== undefined ) {
            var newElement = document.createElement( newTagName );
            newElement.style.display = 'none'
            if ( routeString !== '' ) {
                newElement.setAttribute( 'route', routeString );
            }
            this.shadowRoot.querySelector( '#content-container' ).appendChild( newElement );
            var routeItem = {};
            routeItem.route = routeString;
            routeItem.pageName = node.pageName;
            this._pages.push( routeItem );
        }
        var children = node.childNodes;
        if ( children.length > 0 ) {
            for ( var i = 0; i < children.length; i++ ) {
                var child = children[ i ];
                if ( child.nodeName.toLowerCase() !== 'od-page-router-item' ) {
                    continue;
                }
                var route = routeString;
                if ( child.route !== undefined && child.route !== null ) {
                    if ( routeString === '' ) {
                        route = child.route;
                    } else {
                        route = routeString + '/' + child.route;
                    }                            
                }
                this._buildPagesLoop( child, route );
            }
        }
    }

    _setDefaultRoute() {               
        var temp = this._route.path;
        temp = temp.replace( '/' + this.baseRoute.substr( 0, this.baseRoute.length - 1 ), '' );
        if( temp === '' || temp === '/' ) {
            this._loadComplexRoute( this.defaultRoute );
        } else if ( temp[0] === '/' ) {
            this._loadComplexRoute( temp.substr( 1, temp.length - 1 ) );
        } else {
            this._loadComplexRoute( temp );
        }
    }

    _setLoaded( _loadRoute ) {
        if( _loadRoute === null ) {
            return;
        }
        var temp = '/' + this.baseRoute.substr( 0, this.baseRoute.length - 1 );
        if( this.loadedRoute === _loadRoute || temp + this.loadedRoute === _loadRoute ){
            return;
        }
        if( this.baseRoute === '/' ) {
            this.loadedRoute = _loadRoute;
        } else if ( _loadRoute === temp ) {
            this.loadedRoute = '/' + this.defaultRoute;
        } else if ( _loadRoute.includes( temp ) ) {
            this.loadedRoute = _loadRoute.replace( temp, '' );
        }                
    }

    _routeChangeEvent( e ) {
        e.stopPropagation();
        var route = e.detail.route;
        var params = e.detail.params;
        if( !route || route === '' ) {
            return;
        }
        this._loadComplexRoute( route );
        this._queryParams = params;    
    }

    _loadSimpleRoute() {
        var temp = this.loadRoute;
        if ( temp[0] === '/' ) {
            temp = temp.substr( 1, temp.length - 1 );
        }
        if ( !temp || temp === this.routeData.area + this.tailData.path ) {
            return;
        }
        this._queryParams = {};
        this._loadComplexRoute( temp );          
    }

    _loadComplexRoute( route ) {
        if( this.baseRoute === '/' ) {
            this._loadRoute = route;
        } else {
            this._loadRoute = this.baseRoute + route;
        }
    }

    _urlRoutePageChanged( prefix, params ) {
        if( this.baseRoute !== '/' && !this._route.path.includes( this.baseRoute ) ){
            return;
        }
        if ( prefix === undefined || prefix === null || prefix === '' ) {
            return;
        }
        if( this._pages === null || this._pages === undefined || this._pages === [] || this._pages.length === 0 ) {
            afterNextRender( this, () => { this._urlRoutePageChanged( prefix, params ); } );
            return;
        }
        var paths = this.tailData.path.split('/');
        var baseRoute = this.routeData.area;
        if( this._findRouteLoop( baseRoute, paths ) === false ) {
            for ( var i in this._pages ) {
                var page = this._pages[i];
                if ( page.route === 'not-found' ) {
                    this._processPage( page ); 
                    break;
                }                       
            }                    
        }
    }

    _findRouteLoop( currentRoute, paths ){
        if( paths.length > 0 ) {
            var path = paths.shift();
            var testRoute = currentRoute;
            if( path && path !== '' ) {
                testRoute = currentRoute + '/' + path;
            }
            if ( this._findRouteLoop( testRoute, paths ) === true ) {
                return true;
            }
            for ( var i in this._pages ) {
                var page = this._pages[i];
                if ( page.route === testRoute ) {
                    this._processPage( page );                            
                    return true;
                }                        
            }        
        }
        return false;
    }

    _processPage( page ) {
        this._hideCurrent(); 
        this._importPage( page.pageName );                   
        this._showRelated( page.route );             
    }

    _hideCurrent() {
        if( this._page !== null && this._page !== undefined ) {
            this._page.style.display = 'none'
        }
    }

    _importPage( pageName ) {
        if( this.lazyPages[pageName] ) {
            this.lazyPages[pageName]();       
        } else {
            this.errorPages['notFound'](); 
        }
    }

    _showRelated( route ) {
        var contentContainer = this.shadowRoot.querySelector( '#content-container' );
        var content = contentContainer.childNodes;
        if( this._pages.length !== 0 && content.length === 0 ) {
            afterNextRender( this, () => { this._showRelated( route ); } );
        }
        for ( var i = 0; i < content.length; i++ ) {
            var item = content[i];
            var itemRoute = item.getAttribute( 'route' );
            if ( itemRoute === route ) {
                this._page = item;
                this._page.params = this._queryParams;
                item.style.display = 'block'
                if( item.reload !== undefined ) {
                    item.reload();
                } 
                return;
            }
        }
    }

    _toTop() {
        // For Chrome, Firefox, IE and Opera
        document.documentElement.scrollTop = 0;
        // For Safari
        document.body.scrollTop = 0;
    }

    _removeCodeParam() {
        if( this._queryParams.code ) {
            var params = this._queryParams;
            delete params.code;
            this._queryParams = {};
            this._queryParams = params;

        }
    }
}
customElements.define( OdPageRouter.is, OdPageRouter );