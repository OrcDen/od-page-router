import { PolymerElement, html } from '@polymer/polymer';

export class OdPageRouterItem extends PolymerElement {
    static get is() { return 'od-page-router-item' }
    static get template() { return html`
            <style></style>

            <slot></slot>
        `;
    }
    static get properties() {
        return {
            route: String,

            pageName: String,

            newTagName: String
        }
    }
}
customElements.define( OdPageRouterItem.is, OdPageRouterItem );